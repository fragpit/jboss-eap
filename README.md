# Role bootstrap
## Prepare environment
1. Configure inventory `inventory/hosts` jboss section
2. Configure motd file `roles/bootstrap/templates/motd` 
3. Check default vars in `group_vars/all`

`ntp_servers` - custom NTP servers for chrony  
`users` - cutsom users to add and test  

>  sshpass tool need to be installed on ansible machine for the ssh login test.  

## Run playbook
`ansible-playbook -i inventory/hosts bootstrap.yml -k`  

# Role JBOSS EAP
## Prepare environment
1. Configure inventory `inventory/hosts` centos section  
2. Check default vars in `roles/jboss/vars/main.yml`  

`jboss_eap_url` - link to my evaluation version. It will expire in some time.  
You can put *.jar installer in playbook root folder, and ansible will use it instead of downloading new version.   

## Run playbook

`ansible-playbook -i inventory/hosts jboss.yml -k`  

`-k` - used for password prompt  
